const express = require("express");
const recordRoutes = express.Router();
const dbo = require("../db/conn");
const ObjectId = require("mongodb").ObjectId;

recordRoutes.route("/products").get(async function(req, res) {
    let db_connect = dbo.getDb("mydb");
    let filter = req.body.filter
    let sort = req.body.sort
    await db_connect.collection("products").find(filter).sort(sort).toArray(function(err, result) {
        if (err) throw err;
        res.json(result);
    });
});

recordRoutes.route("/products").post(async function(req, response){
    let db_connect = dbo.getDb("mydb");
    let myobj = {
        nazwa: req.body.nazwa,
        cena: req.body.cena,
        opis: req.body.opis,
        ilosc: req.body.ilosc,
        jednostka_miary: req.body.jednostka_miary
    };
    const existingProduct = await db_connect.collection("products").findOne({"nazwa": myobj.nazwa});
    if (existingProduct) {
      console.log("Nazwa nie jest unikalna!");
      response.json("Nazwa nie jest unikalna!")
    } else {
        db_connect.collection("products").insertOne(myobj, function(err, result){
        if (err) throw err;
        response.json(result);
    });
    }
});

recordRoutes.route("/products/:id").put(async function(req, response){
    let db_connect = dbo.getDb("mydb");
    const id = req.params.id
    const updated = req.body
    const objectId = new ObjectId(id);

    const doesExist = await db_connect.collection("products").findOne({"_id": objectId})
    if (!doesExist) {
        console.log("Nie istnieje rekord o tym id!")
        response.json("Nie istnieje rekord o tym id!")
    } else {
        db_connect.collection("products").updateOne({"_id": objectId}, updated, function(err, result){
            if (err) throw err;
            response.json(result)
        });
    }
})

recordRoutes.route("/products/:id").delete(async function (req, response) {
    let db_connect = dbo.getDb("mydb");
    const id = req.params.id
    const objectId = new ObjectId(id);

    const doesExist = await db_connect.collection("products").findOne({"_id": objectId})

    if (!doesExist) {
        console.log("Nie istnieje rekord o tym id!")
        response.json("Nie istnieje rekord o tym id!")
    } else {
        db_connect.collection("products").deleteOne({"_id": objectId}, function(err, result){
        if (err) throw err;
        response.json(result);
    });
    }
    
})

recordRoutes.route("/products/report").get(async function (req, response) {
    let db_connect = dbo.getDb("mydb");
    
    await db_connect.collection("products").aggregate([
    {'$project': {'_id': 1, 'nazwa': 1, 'cena':1, 'ilosc': 1, 'laczna_warotsc': {'$sum': {'$multiply': ['$ilosc', '$cena']}}}},
    {'$sort': {'_id': 1}}
    ]).toArray(function(err, result) {
            if (err) throw err;
            response.json(result);
    })
})


module.exports = recordRoutes;
